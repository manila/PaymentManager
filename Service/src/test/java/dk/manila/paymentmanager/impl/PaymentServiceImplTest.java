package dk.manila.paymentmanager.impl;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.junit.Test;

import dk.manila.paymentmanager.exception.PaymentException;
import dk.manila.paymentmanager.queue.Event;
import dk.manila.paymentmanager.queue.EventSender;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.AccountInfo;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.User;

/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 * @author Toonw
 *
 */
public class PaymentServiceImplTest {
	
	private PaymentServiceImpl paymentService; 
	private EventSender sender;	
	
	@Test
	public void givenPayerTokenIdAndPayeeCprAndAmount_returnTransferWasOK() throws Exception {
		//arrange	
		sender = new EventSender() {
			@Override
			public void sendEvent(Event event) throws IOException, TimeoutException {
				Event e = new Event("cpr_by_token_id_request_event_payment_to_customerCallback");
				e.addArg("0303913232");
				
				paymentService.receiveEvent(e);		
			}
		};
		
		FakeBankService fakeBankService = new FakeBankService();
		
		paymentService = new PaymentServiceImpl(sender);
		
		//act
		paymentService.pay(fakeBankService, "11pp22pp33pp", "0102889911", "22.99");	
		
		//assert
		assertEquals(1, fakeBankService.getNumberOfCallsTo_TransferMoneyFromTo()); 
	}
	
	@Test(expected = PaymentException.class)
	public void givenUsedTokenIdToPay_returnTransferWasBad() throws Exception {
		//arrange	
		sender = new EventSender() {
			@Override
			public void sendEvent(Event event) throws IOException, TimeoutException {
				Event e = new Event("cpr_by_token_id_request_event_payment_to_customerCallback");
				e.setSuccessful(false);
				paymentService.receiveEvent(e);		
			}
		};
		
		FakeBankService fakeBankService = new FakeBankService();
		
		paymentService = new PaymentServiceImpl(sender);
		
		//act
		paymentService.pay(fakeBankService, "11pp22pp33pp", "0102889911", "22.99");	 
	}
	
	private class FakeBankService implements BankService{
		private int numberOfCalls_ToTransferMoneyFromTo = 0;
		public int getNumberOfCallsTo_TransferMoneyFromTo() {
			return numberOfCalls_ToTransferMoneyFromTo;
		}
		@Override
		public Account getAccount(String arg0) throws BankServiceException_Exception {
			return null;
		}
		@Override
		public Account getAccountByCprNumber(String cpr) throws BankServiceException_Exception {
			if(cpr.equals("0303913232")) {
				Account account = new Account();
				account.setId("1234567890");
				return account;
			}else {
				Account account = new Account();
				account.setId("0987654321");
				return account;
			}
		}
		@Override
		public String createAccountWithBalance(User arg0, BigDecimal arg1) throws BankServiceException_Exception {
			return null;
		}
		@Override
		public void retireAccount(String arg0) throws BankServiceException_Exception { }
		@Override
		public List<AccountInfo> getAccounts() { return null; }
		@Override
		public void transferMoneyFromTo(String arg0, String arg1, BigDecimal arg2, String arg3)	throws BankServiceException_Exception {
			numberOfCalls_ToTransferMoneyFromTo++;
		}
	}
}
