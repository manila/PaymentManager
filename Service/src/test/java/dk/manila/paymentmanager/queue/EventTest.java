package dk.manila.paymentmanager.queue;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class EventTest {
	Event e;
	
	@Before
	public void beforeEach() {
		e  = new Event("on_event_instruction");
	}
	
	@Test
	public void givenNewEventWithInstructionCreated_returnEventHasTheSameInstruction() {		
		assertEquals("on_event_instruction", e.getInstruction());
	}
	
	@Test
	public void givenNewEventWithInstructionCreated_returnTheDefaultMessageIsTheString_Success() {		
		assertEquals("Success", e.getMessage());
	}
	
	@Test
	public void givenSettingAMessageToAnEvent_returnTheSetMessage() {		
		e.setMessage("Not good");
		assertEquals("Not good", e.getMessage());
	}
	
	@Test
	public void givenNewEventWithInstructionCreated_returnTheDefaultSuccessfulIsTrue() {		
		assertEquals(true, e.isSuccessful());
	}
	
	@Test
	public void givenSettingTheEventSuccessBooleanToFalse_returnFalse() {		
		e.setSuccessful(false);
		assertEquals(false, e.isSuccessful());
	}
	
	@Test
	public void givenNewEventWithInstructionCreated_returnTheDefaultArgumentsAreZero() {		
		assertEquals(0, e.getArgs().size());
	}
	
	@Test
	public void givenAddingNumberOfObjectsInTheArgumentsBag_returnTheNumberOfObjects() {
		e.addArg(new Event("on_new_element_added"));
		e.addArg(new Event("on_more_elements_added"));		
		assertEquals(2, e.getArgs().size());
	}
		
	@Test
	public void givenAddingObjectsOfDifferentTypeToTheBag_returnTheTypesAreOk() {
		e.addArg(new Event("on_different_element_types_added"));
		e.addArg(new TestClass());
		
		assertEquals(true, e.getArgs().get(0) instanceof Event);
		assertEquals(true, e.getArgs().get(1) instanceof TestClass);
	}	
	private class TestClass{}
}
