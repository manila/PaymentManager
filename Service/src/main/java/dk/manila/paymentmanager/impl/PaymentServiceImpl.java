package dk.manila.paymentmanager.impl;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

import dk.manila.paymentmanager.exception.PaymentException;
import dk.manila.paymentmanager.interfaces.PaymentService;
import dk.manila.paymentmanager.queue.Event;
import dk.manila.paymentmanager.queue.EventReceiver;
import dk.manila.paymentmanager.queue.EventSender;
import dtu.ws.fastmoney.BankService;

/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */

public class PaymentServiceImpl implements PaymentService, EventReceiver {
	private EventSender eventSender;
	private CompletableFuture<Event> future;

	/**
	 * Constructor
	 * 
	 * @param eventSender
	 *            allowed object is {@link EventSender}
	 */
	public PaymentServiceImpl(EventSender eventSender) {
		this.eventSender = eventSender;
	}

	protected void setEventSender(EventSender eventSender) {
		this.eventSender = eventSender;
	}

	@Override
	public void pay(BankService bankService, String payerTokenId, String payeeCprNumber, String amount)
			throws PaymentException, Exception {

		Event event = new Event("cpr_by_token_id_request_event_payment_to_customer");
		event.addArg(payerTokenId);

		future = new CompletableFuture<>();
		eventSender.sendEvent(event);

		Event callbackEvent = future.join();

		if (!callbackEvent.isSuccessful())
			throw new PaymentException(callbackEvent.getMessage());

		String payerCpr = (String) callbackEvent.getArgs().get(0);

		String payerAccountId = bankService.getAccountByCprNumber(payerCpr).getId();
		String payeeAccountId = bankService.getAccountByCprNumber(payeeCprNumber).getId();

		bankService.transferMoneyFromTo(payerAccountId, payeeAccountId, new BigDecimal(amount), payerTokenId);
	}

	@Override 
	public void receiveEvent(Event event) {
		if (event.getInstruction().equals("cpr_by_token_id_request_event_payment_to_customerCallback"))
			future.complete(event);
	} 

}
 