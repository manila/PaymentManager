package dk.manila.paymentmanager.interfaces;

import dtu.ws.fastmoney.BankService;

/**
 * This class is used to implement Payment functionality of the system
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public interface PaymentService {

	/**
	 * Transfers money from one account to another via a bank service
	 * {@link BankService} by an amount {@link String} by using CPRnumber
	 * {@link String} of the customer and CPRnumber {@link String} of merchant and
	 * tokenID {@link String} that customer provides
	 * 
	 * @param bankService
	 *            allowed object is {@link BankService}
	 * @param payerTokenId
	 *            allowed object is {@link String}
	 * @param payeeCprNumber
	 *            allowed object is {@link String}
	 * @param amount
	 *            allowed object is {@link String}
	 * @throws Exception
	 */
	void pay(BankService bankService, String payerTokenId, String payeeCprNumber, String amount) throws Exception;

}
