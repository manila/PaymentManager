package dk.manila.paymentmanager.exception;

/**
 * Exception throws when a CprNumber is not correct
 * 
 * @author sebastiannyolm
 *
 */
public class PaymentException extends RuntimeException {

	private static final long serialVersionUID = -7748609687044491887L;

	/**
	 * Constructor of the class CprException
	 * 
	 * @param msg
	 *            message of the exception
	 */
	public PaymentException(String msg) {
		super(msg);
	}
}
