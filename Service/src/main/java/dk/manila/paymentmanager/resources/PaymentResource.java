package dk.manila.paymentmanager.resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Representation of the payment operation 
 * This is used when payment needs to be presented
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
@XmlRootElement
public class PaymentResource {
	@XmlElement public String payerTokenId;
	@XmlElement public String payeeCpr;
	@XmlElement public String amount;
}
