package dk.manila.paymentmanager.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import dk.manila.paymentmanager.interfaces.PaymentService;

/**
 * This class is used to establish listener of the rabbitmq connection
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 * Inspired by the implementation given by Hubert Baumeister
 */
public class RabbitMqListener {
	private EventReceiver service;

	/**
	 * Constructor
	 * @param service allowed object is {@link EventReceiver}
	 */
	public RabbitMqListener(EventReceiver service) {
		this.service = service;
	}

	/**
	 * Establish connection with the rabbitmq server
	 * and listens from the queue with queuename {@link String}
	 * @param exchangeName allowed object is {@link String}
	 * @param exchangeHost allowed object is {@link String}
	 * @param exchangType allowed object is {@link String}
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public void listen(String exchangeName, String exchangeHost, String exchangType)
			throws IOException, TimeoutException {
		ConnectionFactory connectionFactory = new ConnectionFactory(); 
		connectionFactory.setHost(exchangeHost); 
		Connection connection = connectionFactory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(exchangeName, exchangType);
		channel.queueDeclare(exchangeName, false, false, false, null);

		String queueName = channel.queueDeclare().getQueue();
		channel.queueBind(queueName, exchangeName, "");

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {

			String message = new String(delivery.getBody());
			Event event = new Gson().fromJson(message, Event.class);

			service.receiveEvent(event);
		};

		channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
		});
	}
}
