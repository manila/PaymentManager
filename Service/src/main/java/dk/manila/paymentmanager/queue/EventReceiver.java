package dk.manila.paymentmanager.queue;

/**
 * This class is used for receiving events {@link Event}
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 * Inspired by the implementation given by Hubert Baumeister
 */
public interface EventReceiver {
	
	/**
	 * Receives an event {@link Event} that is sent by other microservices if event is relevant
	 * completes the {@link CompletableFuture} for event {@link Event}
	 * @param event allowed object is {@link Event}
	 */
	void receiveEvent(Event event);
}
