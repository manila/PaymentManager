package dk.manila.paymentmanager.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * This class is used for sending events {@link Event} to other microservices
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug Inspired by the
 *         implementation given by Hubert Baumeister
 */
public interface EventSender {

	/**
	 * Sends an event {@link Event} to other microservices by using queque of
	 * rabbitmq server
	 * 
	 * @param event
	 *            allowed object is {@link Event}
	 * @throws IOException
	 * @throws TimeoutException
	 */
	void sendEvent(Event event) throws IOException, TimeoutException;

}
