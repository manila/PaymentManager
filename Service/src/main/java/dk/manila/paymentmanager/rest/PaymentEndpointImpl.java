package dk.manila.paymentmanager.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.manila.paymentmanager.exception.PaymentException;
import dk.manila.paymentmanager.interfaces.PaymentService;
import dk.manila.paymentmanager.resources.PaymentResource;
import dtu.ws.fastmoney.BankServiceService;

/**
 * Endpoint of a PaymentManager
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
@Path("/payments")
public class PaymentEndpointImpl {
	private PaymentService service ;
	private String SERVICE_KEY = "service";
	
	/**
	 * Constructor of a PaymentEndPointImpl
	 * @param app allowed object is {@link Application}
	 */
	public PaymentEndpointImpl(@Context Application app) {
		System.out.println(app.getProperties().get(SERVICE_KEY));
		service = (PaymentService) app.getProperties().get(SERVICE_KEY);
		System.out.println(service);
	}	

	/**
	 * 
	 * Calls the Payment Service to make transaction coming from a REST request.
	 * 
	 * @param paymentRequest allowed object is {@link PaymentResource}
	 * @return possible object is {@link Response}
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response createPayment(PaymentResource paymentRequest) {
		
		try {
			service.pay(new BankServiceService().getBankServicePort(), paymentRequest.payerTokenId, paymentRequest.payeeCpr, paymentRequest.amount);
		} catch (PaymentException e) {
			return Response.status(400).entity(e.getMessage()).build();
		} catch (Exception e) {
			return Response.status(400).entity(e.getMessage()).build();
		}
		return Response.ok("Payment successful").build();
	}
}
