package dk.manila.paymentmanager.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import dk.manila.paymentmanager.impl.PaymentServiceImpl;
import dk.manila.paymentmanager.interfaces.PaymentService;
import dk.manila.paymentmanager.queue.EventReceiver;
import dk.manila.paymentmanager.queue.RabbitMqListener;
import dk.manila.paymentmanager.queue.RabbitMqSender;

/**
 * Application that starts the REST interface, creates a PaymentService {@link PaymentService}
 *  and starts the listener of queue
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
@ApplicationPath("/")
public class RestApplication extends Application {
	private final String EXCHANGE_NAME = "dtu_pay_exchange";
	private final String EXCHANGE_HOST = "rabbitmq";
	private final String EXCHANGE_TYPE = "fanout";
	private PaymentService service;	
	
	/** 
	 * Constructor of RestApplication in which we start a REST interface, create a
	 * PaymentService {@link PaymentService} and start the listener of queue.
	 */
	public RestApplication() {
		service = new PaymentServiceImpl(new RabbitMqSender(EXCHANGE_NAME, EXCHANGE_HOST, EXCHANGE_TYPE));
		try {
			new RabbitMqListener((EventReceiver) service).listen(EXCHANGE_NAME, EXCHANGE_HOST, EXCHANGE_TYPE);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> map = new HashMap<>();
		map.put("service", service);
		return map;
	}
}
