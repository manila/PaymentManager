package dk.manila.paymentmanager.rest;


import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;


/**
 * Endpoint used to ping if the service is working
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
@Path("/hello")
public class HelloWorldEndpoint {

	/**
	 * Returns an HTTP response 200 if the service can be reached.
	 * 
	 * @return possible object is
     *     {@link Response}
	 */
	@GET
	@Produces("text/plain")
	public Response doGet() {
		return Response.ok("Hello from Payment Manager").build();
	}
}
