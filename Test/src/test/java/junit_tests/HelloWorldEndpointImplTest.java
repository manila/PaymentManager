package junit_tests;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Test;

/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 * @author Toonw
 *
 */
public class HelloWorldEndpointImplTest {
	
	@Test
	public void givenGetHello_returnHelloString() {		
		Client client = ClientBuilder.newClient();		
		WebTarget webTarget = client.target("http://02267-manila.compute.dtu.dk:8001");
		webTarget = webTarget.path("hello");
		
		Response response = webTarget.request().get();		
		assertEquals(200, response.getStatus());
		assertEquals("Hello from Payment Manager", response.readEntity(String.class));
	}	

}